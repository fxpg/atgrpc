#!/bin/sh

for var in front_server master_server
do
  python -m grpc_tools.protoc -I ./$var --python_out=./$var --grpc_python_out=./$var ./$var/*.proto
	protoc --proto_path=./$var --go_out=plugins=grpc:./$var ./$var/*.proto
done
