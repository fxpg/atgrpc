

## MASTER_SERVER( host:50061 )

### FRONT_SERVER >>

- RunTrader(userId int,traderId int,token string, code string, tick_sec int) (isRunned bool)
- RunBacktest(userId int, backtestId int, token string, code string, tick_sec int,from timestamp, to timestamp) (isRunned bool)

### WORKER >>

- Tick(exchange string, pair string, duration int) Tick
- Charts(exchange string, pair string, duration int, num int) Tick[]
- Balance(userId int, token string)(balances Balance[])
- Order(userId int, traderId int, token string, exchange string, pair string, asset_rate float, order_type int) bool
- Error(traderId int, error ErrorType) empty
  enum ErrorType {
    Timeout = 0;
    CompileFailed = 1;
    Others = 2;
  }
- WriteLogs([]Log, userId string, traderId string) empty

#### Log

- datetime
- line

#### Tick

- open
- close
- high
- low
- volume
- timestamp(unix)

---

## FRONT_SERVER( host:50051 )

### MASTER_SERVER >>

- Tick(exchange string, pair string, duration int) Tick
- Charts(exchange string, pair string, duration int, num int) Tick[]
- InsertOrderData(userId int, traderId int, datetime(unix) int, exchange string, pair string, amount float, price float, order_type int) bool
- Auth(userId int, token string) bool
- APIkey(exchange string, user_id int) (api_key string, secret_key string)

- Error(traderId int, error ErrorType) empty
  enum ErrorType {
    Timeout = 0;
    CompileFailed = 1;
    Others = 2;
  }
- WriteLogs([]Log) empty
